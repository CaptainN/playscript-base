using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace PlayScript.Tests.Application
{
	public class ApplicationMain
	{
		// This is the main entry point of the application.
		public static void Main(string[] args)
		{
//			playscript.Application.run(args, typeof(_root.Startup));
			PlayScript.Tests.Performance.PerfTest.DoDynamicTest();
		}
	}
}
