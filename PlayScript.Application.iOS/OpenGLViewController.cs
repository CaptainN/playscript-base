using System;

using OpenTK;
using OpenTK.Graphics.ES20;
using OpenTK.Platform.iPhoneOS;

using MonoTouch.Foundation;
using MonoTouch.CoreAnimation;
using MonoTouch.ObjCRuntime;
using MonoTouch.OpenGLES;
using MonoTouch.UIKit;

namespace PlayScript.Application.iOS
{
	[Register ("OpenGLViewController")]
	public partial class OpenGLViewController : UIViewController
	{
		public OpenGLViewController (string nibName, NSBundle bundle) : base (nibName, bundle)
		{
		}
		
		new EAGLView View { get { return (EAGLView)base.View; } }
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// We are not using NSNotificationCenter.DefaultCenter.AddObserver()
			// as it did not send the expected events. Instead we only catch it at the AppDelegate level

			SetupPinchGestureRecognizer();
		}
		
		protected override void Dispose (bool disposing)
		{
			base.Dispose (disposing);
			
			NSNotificationCenter.DefaultCenter.RemoveObserver (this);
		}
		
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			View.StartAnimating ();
		}
		
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			View.StopAnimating ();
		}

		private void OnPinchRecongnized(UIPinchGestureRecognizer pinchRecognizer)
		{
			View.OnPinchRecognized(pinchRecognizer);
		}

		private void SetupPinchGestureRecognizer()
		{
			// create a new pinch gesture
			UIPinchGestureRecognizer gesture = new UIPinchGestureRecognizer ();
			// wire up the event handler (have to use a selector)
			gesture.AddTarget ( () => { OnPinchRecongnized(gesture); });
			// add the gesture recognizer to the view
			View.AddGestureRecognizer(gesture);
		}

		public void Trace(string text)
		{
			// Debug text
			// TODO: Remove this before we ship.
			for ( int i = 0 ; i < 5 ; ++i)
			{
				Console.WriteLine(text);
			}
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public void OnActivated()
		{
			Trace("OnActivated() called.");

			View.RenderFrameEnabled = true;
			if (IsViewLoaded && View.Window != null)
				View.StartAnimating ();
		}

		public void DidEnterBackground()
		{
			Trace("DidEnterBackground() called.");
		}

		public void WillEnterForeground()
		{
			Trace("WillEnterForeground() called.");

			View.RenderFrameEnabled = true;

			if (IsViewLoaded && View.Window != null) 
			{
				View.StartAnimatingFromInactive();
			}
		}

		public void OnResignActivation()
		{
			Trace("OnResignActivation() called.");

			View.RenderFrameEnabled = false;

			if (IsViewLoaded && View.Window != null) 
			{
				View.StopAnimatingGoingInactive ();
			}
		}

		public void WillTerminate()
		{
			Trace("WillTerminate() called.");

			View.RenderFrameEnabled = false;
			if (IsViewLoaded && View.Window != null)
				View.StopAnimating ();
		}
	}
}
