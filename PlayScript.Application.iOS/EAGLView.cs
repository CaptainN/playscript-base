using System;
using System.Collections.Generic;

using OpenTK;
using OpenTK.Graphics.ES20;
using GL1 = OpenTK.Graphics.ES11.GL;
using All1 = OpenTK.Graphics.ES11.All;
using OpenTK.Platform.iPhoneOS;

using MonoTouch.Foundation;
using MonoTouch.CoreAnimation;
using MonoTouch.ObjCRuntime;
using MonoTouch.OpenGLES;
using MonoTouch.UIKit;

namespace PlayScript.Application.iOS
{
	[Register ("EAGLView")]
	public class EAGLView : iPhoneOSGameView
	{
		// this is our playscript player
		PlayScript.Player	mPlayer;
		public bool			RenderFrameEnabled = true;

		[Export("initWithCoder:")]
		public EAGLView (NSCoder coder) : base (coder)
		{
			AutoResize = true;
			LayerRetainsBacking = false;
			LayerColorFormat = EAGLColorFormat.RGBA8;
		}
		
		[Export ("layerClass")]
		public static new Class GetLayerClass ()
		{
			return iPhoneOSGameView.GetLayerClass ();
		}
		
		protected override void ConfigureLayer (CAEAGLLayer eaglLayer)
		{
			eaglLayer.Opaque = true;

			// did player specify an content scale?
			if (PlayScript.Player.ApplicationContentScale.HasValue) {
				// set content scale 
				eaglLayer.ContentsScale = (float)PlayScript.Player.ApplicationContentScale.Value; 
			} else {
				if (this.Frame.Width <= 480.0 || this.Frame.Height <= 480.0) {
					if (PlayScript.IOSDeviceHardware.Version < PlayScript.IOSDeviceHardware.IOSHardware.iPhone4S) {
						// Non-retina for iPhone4 and below for the moment
						eaglLayer.ContentsScale = 1.0f;
					} else {
						eaglLayer.ContentsScale = 2.0f; // UIScreen.MainScreen.Scale;
					}
				} else {
					eaglLayer.ContentsScale = 1.0f; // UIScreen.MainScreen.Scale;
				}
			}

			Console.WriteLine("context scale for frame {0} set to {1}", this.Frame, eaglLayer.ContentsScale);
		}
		
		protected override void CreateFrameBuffer ()
		{
			try {
				ContextRenderingApi = EAGLRenderingAPI.OpenGLES2;
				base.CreateFrameBuffer ();
			} catch (Exception) {
				try {
					ContextRenderingApi = EAGLRenderingAPI.OpenGLES1;
					base.CreateFrameBuffer ();
				} catch (Exception e) {
					// Print an error message before rethrowing
					Console.WriteLine("***ERROR*** Could not create frame buffer");
					throw e;
				}

			}
		}
		
		protected override void DestroyFrameBuffer ()
		{
			base.DestroyFrameBuffer ();
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
#if PLATFORM_MONODROID
			mPlayer.OnTouchesBegan( CreateTouchEvents(flash.events.TouchEvent.TOUCH_BEGIN, touches) );
#else
			mPlayer.OnTouchesBegan(touches, evt);
#endif
		}

		public override void TouchesMoved (NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);
#if PLATFORM_MONODROID
			mPlayer.OnTouchesMoved( CreateTouchEvents(flash.events.TouchEvent.TOUCH_MOVE, touches) );
#else
			mPlayer.OnTouchesMoved (touches, evt);		
#endif
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
#if PLATFORM_MONODROID
			mPlayer.OnTouchesEnded( CreateTouchEvents(flash.events.TouchEvent.TOUCH_END, touches) );
#else
			mPlayer.OnTouchesEnded(touches, evt);
#endif
		}

		public void OnPinchRecognized(UIPinchGestureRecognizer pinchRecognizer)
		{
#if PLATFORM_MONODROID
			flash.events.TransformGestureEvent tge = new flash.events.TransformGestureEvent(flash.events.TransformGestureEvent.GESTURE_ZOOM, true, false);
			tge.scaleX = tge.scaleY = pinchRecognizer.Scale;

			switch (pinchRecognizer.State) 
			{
				case UIGestureRecognizerState.Possible:
				case UIGestureRecognizerState.Began:
					tge.phase = "begin";
					break;
				case UIGestureRecognizerState.Changed:
					tge.phase = "update";
					break;

				case UIGestureRecognizerState.Recognized:
					tge.phase = "end";
					break;

				case UIGestureRecognizerState.Cancelled:
				case UIGestureRecognizerState.Failed:
					return;
			}

			mPlayer.OnPinchRecognized(tge);
#else
			mPlayer.OnPinchRecognized(pinchRecognizer);
#endif
		}

		private List<flash.events.TouchEvent> CreateTouchEvents(String action, NSSet touches)
		{
			List<flash.events.TouchEvent> results = new List<flash.events.TouchEvent> ();

			foreach (UITouch uiTouch in touches) {
				System.Drawing.PointF p = uiTouch.LocationInView (uiTouch.View);
				// convert point to pixels
				float scale = uiTouch.View.ContentScaleFactor;
				p.X *= scale;
				p.Y *= scale;

				var touchEvent = new flash.events.TouchEvent(action, true, false, 0, true, p.X, p.Y, 1.0, 1.0, 1.0 );

				results.Add (touchEvent);
			}

			return results;
		}
		
		#region DisplayLink support
		
		int frameInterval;
		CADisplayLink displayLink;
		
		public bool IsAnimating { get; private set; }
		
		// How many display frames must pass between each time the display link fires.
		public int FrameInterval {
			get {
				return frameInterval;
			}
			set {
				if (value <= 0)
					throw new ArgumentException ();
				frameInterval = value;
				if (IsAnimating) {
					StopAnimating ();
					StartAnimating ();
				}
			}
		}

		public void StartAnimating ()
		{
			if (IsAnimating)
				return;
			
			CreateFrameBuffer ();
			CADisplayLink displayLink = UIScreen.MainScreen.CreateDisplayLink (this, new Selector ("drawFrame"));
			displayLink.FrameInterval = frameInterval;
			displayLink.AddToRunLoop (NSRunLoop.Current, NSRunLoop.NSDefaultRunLoopMode);
			this.displayLink = displayLink;

			if (mPlayer == null) {
				InitPlayer();
			}

			IsAnimating = true;
		}
		
		public void StopAnimating ()
		{
			if (!IsAnimating)
				return;
			displayLink.Invalidate ();
			displayLink = null;
			DestroyFrameBuffer ();
			IsAnimating = false;
		}

		//
		// re-start animations (and GL rendering) coming out from an inactive state
		public void StartAnimatingFromInactive ()
		{
			if (IsAnimating)
				return;

			CADisplayLink displayLink 	= UIScreen.MainScreen.CreateDisplayLink (this, new Selector ("drawFrame"));
			displayLink.FrameInterval 	= frameInterval;
			displayLink.AddToRunLoop (NSRunLoop.Current, NSRunLoop.NSDefaultRunLoopMode);
			this.displayLink 			= displayLink;
			IsAnimating 				= true;
		}

		//
		// stop animations (and GL rendering) just before going to an inactive state
		public void StopAnimatingGoingInactive ()
		{
			if (!IsAnimating)
				return;

			// flush GL (empty GL command-buffer and cache and wait for GPU to execture all remaining commands)
			GL.Flush ();
			GL.Finish ();

			displayLink.Invalidate ();

			displayLink = null;
			IsAnimating = false;
		}

		[Export ("drawFrame")]
		void DrawFrame ()
		{
			OnRenderFrame (new FrameEventArgs ());
		}
		
		#endregion

		private System.Drawing.RectangleF GetScaledFrame()
		{
			var scale = this.Layer.ContentsScale;
			var rect = this.Bounds;
			rect.X      *= scale;
			rect.Y      *= scale;
			rect.Width  *= scale;
			rect.Height *= scale;

			int width, height;
			GL.GetRenderbufferParameter(RenderbufferTarget.Renderbuffer, RenderbufferParameterName.RenderbufferWidth, out width);
			GL.GetRenderbufferParameter(RenderbufferTarget.Renderbuffer, RenderbufferParameterName.RenderbufferHeight, out height);
			return rect;
		}

		protected void InitPlayer()
		{
			// create player
			var rect = GetScaledFrame();
			mPlayer = new PlayScript.Player(rect);
		}

		protected override void OnRenderFrame (FrameEventArgs e)
		{
			base.OnRenderFrame (e);
			
			MakeCurrent ();

			if (RenderFrameEnabled)
			{
				if (mPlayer != null) {
					// run the player
					var rect = GetScaledFrame();
					mPlayer.RunUntilPresent(rect);
				}

				PlayScript.Profiler.Begin("swap", ".rend.gl.swap");
			
				// discard depth buffer at the end of the frame
				// this is a hint to GL to not keep the data around
				All discard = (All)FramebufferSlot.DepthAttachment;
				GL.Ext.DiscardFramebuffer((All)FramebufferTarget.Framebuffer, 1, ref discard);

				SwapBuffers ();
				PlayScript.Profiler.End("swap");
			}
		}
	}
}
