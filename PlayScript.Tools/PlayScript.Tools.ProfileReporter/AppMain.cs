
using System;
using System.IO;
using System.Diagnostics;
using Mono.Profiler;

namespace PlayScript.Tools.ProfileReporter
{
	public static class AppMain
	{
		public class DefaultHandler : EventHandlerBase
		{
			Stopwatch mTimer = Stopwatch.StartNew();
			double mLastPercent = -1;
			int mFrame = 0;
			int mStackDepth = 0;
			const int MaxStackDepth = 50;
			bool mDoTraceThisFrame = false;
			bool mTraceMasterSwitch = false;

			int mStartAtFrame = 1000;			// We are not writing before N frames
			int mEndAtFrame = 2000;
			bool mWritePerfLog = false;
			bool mWriteMemoryLog = false;
			bool mWriteMemoryOnPerfLog = true;

			static string[] sIndenters = new string[MaxStackDepth];

			static DefaultHandler()
			{
				for (int i = 0 ; i < sIndenters.Length ; ++i)
				{
					sIndenters[i] = new string(' ', i);
				}
			}

			string mDirectory;
			TextWriter mPerfWriter;
			TextWriter mMemoryWriter;

			public DefaultHandler(string directory)
			{
				// If directory is set, it will create a new file for each frame
				Directory.CreateDirectory(directory);
				mDirectory = directory;
			}

			public DefaultHandler(TextWriter writer)
			{
				mPerfWriter = writer;
				mMemoryWriter = writer;
			}

			void CreateWriterIfNeeded()
			{
				if (mFrame < mStartAtFrame)
				{
					return;
				}
				if (mDirectory != null)
				{
					string fileName;

					if (mWritePerfLog || mWriteMemoryOnPerfLog)
					{
						fileName = Path.Combine(mDirectory, "frame_perf#" + mFrame + ".txt");
						mPerfWriter = new StreamWriter(fileName);
					}

					if (mWriteMemoryLog)
					{
						fileName = Path.Combine(mDirectory, "frame_mem#" + mFrame + ".txt");
						mMemoryWriter = new StreamWriter(fileName);
					}
				}
			}

			void ReleaseWriterIfNeeded()
			{
				if (mDirectory != null)
				{
					if ((mWritePerfLog || mWriteMemoryOnPerfLog) && (mPerfWriter != null))
					{
						mPerfWriter.Close();
						mPerfWriter.Dispose();
						mPerfWriter = null;
					}
					if (mWriteMemoryLog && (mMemoryWriter != null))
					{
						mMemoryWriter.Close();
						mMemoryWriter.Dispose();
						mMemoryWriter = null;
					}
				}
			}

			void WriteLinePerf(string format, params object[] args)
			{
				if ((mWritePerfLog || mWriteMemoryOnPerfLog) && (mPerfWriter != null))
				{
					mPerfWriter.WriteLine(format, args);
				}
			}

			void WriteLineMemory(string format, params object[] args)
			{
				if (mWriteMemoryLog && (mMemoryWriter != null))
				{
					mMemoryWriter.WriteLine(format, args);
				}
				if (mWriteMemoryOnPerfLog && (mPerfWriter != null))
				{
					mPerfWriter.WriteLine(format, args);
				}
			}

			#region IEventHandler implementation
			public override void OnProgress(ulong position, ulong length, double percent)
			{
				// print each percent completed
				percent = Math.Floor(percent);
				if (percent != mLastPercent)
				{
					mLastPercent = percent;
					Console.WriteLine("{0:0.00}% completed ({1:0.00}MB/sec)", percent, position * 1000.0 / (1024.0 * 1024.0 * mTimer.ElapsedMilliseconds) );
				}
			}

			public override void OnMethodEnter(ulong time, MethodDesc method)
			{
				if (!mDoTraceThisFrame)
					return;

				if (mStackDepth < MaxStackDepth) {
					WriteLinePerf("{0} {2}enter {1}", time, method, sIndenters[mStackDepth]);
				}
				mStackDepth++;
			}

			public override void OnMethodLeave(ulong time, MethodDesc method)
			{
				if (!mDoTraceThisFrame)
					return;

				mStackDepth--;
				if (mStackDepth < 0)
				{
					mStackDepth = 0;
					Console.WriteLine("Incorrect stack-depth");
				}
				if (mStackDepth < MaxStackDepth) {
					WriteLinePerf("{0} {2}leave {1}", time, method, sIndenters[mStackDepth]);
				}
			}

			public override void OnLoadImage(ulong time, ImageDesc desc)
			{
//				Console.WriteLine("{0} load image {1}", time, desc);
			}

			public override void OnLoadClass(ulong time, ClassDesc desc)
			{
//				Console.WriteLine("{0} load class {1}", time, desc);
			}

			public override void OnMethodCompile(ulong time, MethodDesc desc)
			{
				//				Console.WriteLine("{0} compile {1}", time, desc);

				// setup hooks
				if (desc.Name.StartsWith("PlayScript.Player:OnFrame "))
				{
					// this is called at the beginning of a frame
					desc.OnEnter = (t,m) => 
					{
						if (mTraceMasterSwitch)
						{
							// do a trace for this frame
							mDoTraceThisFrame = true;

							CreateWriterIfNeeded();

							WriteLinePerf("{0} start frame {1}", t, mFrame);
						}
					};

					// this is called at the end of a frame
					desc.OnLeave = (t,m) => 
					{
						if (mTraceMasterSwitch)
						{
							// disable tracing at the end of a frame
							mDoTraceThisFrame = false;

							WriteLinePerf("{0} end frame {1}", t, mFrame);

							ReleaseWriterIfNeeded();
						}
						mFrame++;
					};
				}

				if (desc.Name.StartsWith("PlayScript.Profiler:StartSession"))
				{
					// this is called when starting a profiling session
					desc.OnEnter = (t,m) => 
					{
						// do a trace when we start a profiling session
						mTraceMasterSwitch = true;
						// reset frame count for purposes of matching up frames between the built in profiler and this reporter
						mFrame = -5;
					};
				}
			}

			public override void OnObjectAlloc(ulong time, ClassDesc classDesc, long address, ulong size, MethodDesc[] backTrace)
			{
				if (mDoTraceThisFrame == false)
				{
					return;
				}
				WriteLineMemory("{0} alloc object:{1} size:{2} class:{3}", time, address, size, classDesc);
				// Due to inconsistencies in the callstack, just don't use it.
			}

			public override void OnObjectMove(ulong time, long sourceAddress, long destAddress)
			{
			}
			#endregion
		}

		public static void Main(string[] args)
		{
			// temp since i couldnt get xamarin to pass the right args
			args = new string[] { "/Users/onallet/Desktop/profiler-output.mlpd", "/Users/onallet/Desktop/profiler-output/" };

			if (args.Length < 0) {
				Console.WriteLine("Usage: ProfileReporter <path-to-mlpd> [<outputfile>]");
				return;
			}

			string path = args[0];
			if (!File.Exists(path)) {
				Console.WriteLine("File does not exist: {0}", path);
				return;
			}

			bool compressed = false; // TODO detect this

			var log = new Mono.Profiler.LogReader();
			if (args.Length >= 2) {
				// write to files
				Console.WriteLine("Writing reports to directory: {0}", args[1]);
				log.ParseFile(path, new DefaultHandler(args[1]), compressed);
			} else {
				// write to console
				log.ParseFile(path, new DefaultHandler(System.Console.Out), compressed);
			}

			Console.WriteLine("Completed successfully");
		}
	}
}

