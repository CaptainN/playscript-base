using System;
using System.IO;
using System.Collections.Generic;

using int32_t = System.Int32;
using uint32_t = System.UInt32;
using int64_t = System.Int64;
using uint64_t = System.UInt64;
using intptr_t = System.Int64;
using uintptr_t = System.UInt64;

namespace Mono.Profiler
{
	enum EntryType {
		TYPE_ALLOC,
		TYPE_GC,
		TYPE_METADATA,
		TYPE_METHOD,
		TYPE_EXCEPTION,
		TYPE_MONITOR,
		TYPE_HEAP,
		TYPE_SAMPLE,
	};

	[Flags]
	enum SubType {
		/* extended type for TYPE_HEAP */
		TYPE_HEAP_START  = 0 << 4,
		TYPE_HEAP_END    = 1 << 4,
		TYPE_HEAP_OBJECT = 2 << 4,
		TYPE_HEAP_ROOT   = 3 << 4,
		/* extended type for TYPE_METADATA */
		TYPE_START_LOAD   = 1 << 4,
		TYPE_END_LOAD     = 2 << 4,
		TYPE_START_UNLOAD = 3 << 4,
		TYPE_END_UNLOAD   = 4 << 4,
		TYPE_LOAD_ERR     = 1 << 7,
		/* extended type for TYPE_GC */
		TYPE_GC_EVENT  = 1 << 4,
		TYPE_GC_RESIZE = 2 << 4,
		TYPE_GC_MOVE   = 3 << 4,
		TYPE_GC_HANDLE_CREATED   = 4 << 4,
		TYPE_GC_HANDLE_DESTROYED = 5 << 4,
		/* extended type for TYPE_METHOD */
		TYPE_LEAVE     = 1 << 4,
		TYPE_ENTER     = 2 << 4,
		TYPE_EXC_LEAVE = 3 << 4,
		TYPE_JIT       = 4 << 4,
		/* extended type for TYPE_EXCEPTION */
		TYPE_THROW        = 0 << 4,
		TYPE_CLAUSE       = 1 << 4,
		TYPE_EXCEPTION_BT = 1 << 7,
		/* extended type for TYPE_ALLOC */
		TYPE_ALLOC_BT  = 1 << 4,
		/* extended type for TYPE_MONITOR */
		TYPE_MONITOR_BT  = 1 << 7,
		/* extended type for TYPE_SAMPLE */
		TYPE_SAMPLE_HIT    = 0 << 4,
		TYPE_SAMPLE_USYM   = 1 << 4,
		TYPE_SAMPLE_UBIN   = 2 << 4,
	};

	enum SampleType {
		SAMPLE_CYCLES = 1,
		SAMPLE_INSTRUCTIONS,
		SAMPLE_CACHE_MISSES,
		SAMPLE_CACHE_REFS,
		SAMPLE_BRANCHES,
		SAMPLE_BRANCH_MISSES,
		SAMPLE_LAST
	};

	enum MetadataType
	{
		TYPE_CLASS     = 1,
		TYPE_IMAGE     = 2,
		TYPE_ASSEMBLY  = 3,
		TYPE_DOMAIN    = 4,
		TYPE_THREAD    = 5,
	};

	enum MonitorEvent {
		MONO_PROFILER_MONITOR_CONTENTION = 1,
		MONO_PROFILER_MONITOR_DONE = 2,
		MONO_PROFILER_MONITOR_FAIL = 3
	};

	public class LogReader
	{
		private const int BUF_ID = 0x4D504C01;
		private const int LOG_HEADER_ID = 0x4D505A01;
		private const int LOG_VERSION_MAJOR = 0;
		private const int LOG_VERSION_MINOR = 4;
		private const int LOG_DATA_VERSION = 4;

		// file header
		private uint logHeader;
		private byte version_major;  
		private byte version_minor;  
		private byte data_version;  
		private byte pointer_size;  
		private long startup_time;
		private long timer_overhead;
		private long flags;
		private long pid;
		private long port;

		private Dictionary<long, MethodDesc> mMethods = new Dictionary<long, MethodDesc>();
		private Dictionary<long, ClassDesc> mClasses = new Dictionary<long, ClassDesc>();
		private Dictionary<long, ImageDesc> mImages = new Dictionary<long, ImageDesc>();
		private Dictionary<long, ThreadDesc> mThreads = new Dictionary<long, ThreadDesc>();
		private Dictionary<long, MonitorDesc> mMonitors = new Dictionary<long, MonitorDesc>();

		// event handler for dispatching profiling log events
		private IEventHandler mHandler;

		// byte buffer for reading
		private byte[]		mBytes;
		private int 		mByteIndex;

		private byte ReadByte()
		{
			return mBytes[mByteIndex++];
		}

		private String ReadString()
		{
			int start = mByteIndex;
			// scan until zero terminator
			while (mBytes[mByteIndex] != 0)
				mByteIndex++;

			var str = System.Text.UTF8Encoding.UTF8.GetString(mBytes, start, mByteIndex - start);
			mByteIndex++;
			return str;
		}

		private ulong decode_uleb128()
		{
			ulong res = 0;
			int shift = 0;

			for (;;) {
				byte b = ReadByte();

				res |= (((ulong)(b & 0x7f)) << shift);
				if ((b & 0x80) == 0)
					break;
				shift += 7;
			}
			return res;
		}

		private long	decode_sleb128 ()
		{
			long res = 0;
			int shift = 0;

			for (;;) {
				byte b = ReadByte();

				res = res | (((long)(b & 0x7f)) << shift);
				shift += 7;
				if ((b & 0x80) == 0) {
					if (shift < sizeof (long) * 8 && (b & 0x40)!=0)
						res |= - ((long)1 << shift);
					break;
				}
			}
			return res;
		}

		private ClassDesc lookup_class(long addr)
		{
			ClassDesc desc;
			if (!mClasses.TryGetValue(addr, out desc)) {
				desc = new ClassDesc();
				desc.Name = addr.ToString();
				mClasses.Add(addr, desc);
			}
			return desc;
		}

		private ClassDesc add_class(long addr, string name)
		{
			var desc = lookup_class(addr);
			desc.Name = name;
			return desc;
		}


		private MethodDesc lookup_method(long addr)
		{
			MethodDesc desc;
			if (!mMethods.TryGetValue(addr, out desc)) {
				desc = new MethodDesc();
				desc.Name = addr.ToString();
				mMethods.Add(addr, desc);
			}
			return desc;
		}

		private MethodDesc add_method(long addr, string name, intptr_t code, int len)
		{
			MethodDesc desc = lookup_method(addr);
			desc.Name = name;
			desc.Code = code;
			desc.Length = len;
			return desc;
		}

		private ImageDesc add_image(long addr, string name)
		{
			var desc = new ImageDesc() { Name = name};
			mImages[addr] = desc;
			return desc;
		}
		
		private ThreadDesc lookup_thread(long addr)
		{
			ThreadDesc desc;
			if (!mThreads.TryGetValue(addr, out desc)) {
				desc = new ThreadDesc();
				desc.Id = mThreads.Count + 1;
				desc.Name = desc.Id.ToString();
				mThreads.Add(addr, desc);
			}
			return desc;
		}

		private MonitorDesc lookup_monitor(long addr)
		{
			MonitorDesc desc;
			if (!mMonitors.TryGetValue(addr, out desc)) {
				desc = new MonitorDesc();
				desc.Name = addr.ToString();
				mMonitors.Add(addr, desc);
			}
			return desc;
		}

		private MethodDesc[] decode_bt(intptr_t ptr_base)
		{
			var frames = new List<MethodDesc>();
			int i;
			int flags = (int)decode_uleb128 ();
			int count = (int)decode_uleb128 ();
			if (flags != 0)
				return null;
			for (i = 0; i < count; ++i) {
				intptr_t ptrdiff = decode_sleb128 ();
				frames.Add(lookup_method (ptr_base + ptrdiff));
			}
			return frames.ToArray();
		}

		private long OBJ_ADDR(intptr_t obj_base, intptr_t objdiff)
		{
			return ((obj_base + objdiff) << 3);
		}

		private void ParseBuffer(BinaryReader br)
		{
			uint bufferId = br.ReadUInt32();
			if (bufferId != (uint)BUF_ID) 
			{
				throw new InvalidDataException("Buffer ID is invalid");
			}

			// parse buffer header
			int buffer_length = br.ReadInt32();
			uint64_t time_base = br.ReadUInt64();  
			intptr_t ptr_base = br.ReadInt64();  
			intptr_t obj_base = br.ReadInt64();  
			intptr_t thread_id = br.ReadInt64();  
			intptr_t method_base = br.ReadInt64();  

			ThreadDesc thread = lookup_thread(thread_id);
			mHandler.SetThread(thread);

			// read bytes for buffer
			mBytes = br.ReadBytes(buffer_length);
			if (mBytes.Length < buffer_length) {
				throw new InvalidDataException("Buffer is truncated");
			}

			// read all bytes of buffer
			mByteIndex = 0;
			while (mByteIndex < buffer_length)
			{
				// parse entry type
				byte code = ReadByte();
				EntryType etype = (EntryType)(code & 0x0F);
				SubType subtype = (SubType)(code & 0xF0);

//				Console.WriteLine("{0}", etype);

				switch (etype) 
				{
					case EntryType.TYPE_ALLOC:
					{
						uint64_t tdiff = decode_uleb128();
						time_base += tdiff;

						intptr_t ptrdiff = decode_sleb128();
						intptr_t objdiff = decode_sleb128();
						uint64_t len = decode_uleb128();

						MethodDesc[] frames = null;
						if ((subtype & SubType.TYPE_ALLOC_BT) != 0) {
							 frames = decode_bt(ptr_base);
						}
						mHandler.OnObjectAlloc(time_base, lookup_class(ptr_base + ptrdiff), OBJ_ADDR(obj_base, objdiff), len, frames);
						break;
					}

					case EntryType.TYPE_METHOD: 
					{
						uint64_t tdiff = decode_uleb128 ();
						time_base += tdiff;

						int64_t ptrdiff = decode_sleb128 ();
						method_base += ptrdiff;
						if (subtype == SubType.TYPE_JIT) {
							intptr_t codediff = decode_sleb128 ();
							int codelen = (int)decode_uleb128 ();
							var method = add_method (method_base, ReadString(), ptr_base + codediff, codelen);
							mHandler.OnMethodCompile(time_base, method);
						} else {
							MethodDesc method = lookup_method (method_base);
							if (subtype == SubType.TYPE_ENTER) {
								if (method.OnEnter != null) 
									method.OnEnter(time_base, method);
								mHandler.OnMethodEnter(time_base, method);
							} else {
								mHandler.OnMethodLeave(time_base, method);
								if (method.OnLeave != null) 
									method.OnLeave(time_base, method);
							}
						}
						break;
					}

					case EntryType.TYPE_GC: 
					{
						uint64_t tdiff = decode_uleb128 ();
						time_base += tdiff;

						if (subtype == SubType.TYPE_GC_RESIZE) {
							uint64_t new_size = decode_uleb128 ();
							mHandler.OnGCResize(time_base, new_size);
						} else if (subtype == SubType.TYPE_GC_EVENT) {
							uint64_t ev = decode_uleb128 ();
							int gen = (int)decode_uleb128 ();
							mHandler.OnGCEvent(time_base, gen, (MonoGCEvent)ev);
						} else if (subtype == SubType.TYPE_GC_MOVE) {
							int j;
							int num = (int)decode_uleb128 ();
							for (j = 0; j < num; j += 2) {
								intptr_t obj1diff = decode_sleb128 ();
								intptr_t obj2diff = decode_sleb128 ();
								mHandler.OnObjectMove(time_base, OBJ_ADDR(obj_base, obj1diff), OBJ_ADDR(obj_base, obj2diff));
							}
						} else if (subtype == SubType.TYPE_GC_HANDLE_CREATED) {
							int htype = (int)decode_uleb128 ();
							uint32_t handle = (uint32_t)decode_uleb128 ();
							intptr_t objdiff = decode_sleb128 ();
							if (htype > 3)
								throw new InvalidDataException("Invalid GC handle");
							mHandler.OnGCHandleCreated(time_base, htype, handle,OBJ_ADDR (obj_base, objdiff));
						} else if (subtype == SubType.TYPE_GC_HANDLE_DESTROYED) {
							int htype = (int)decode_uleb128 ();
							uint32_t handle = (uint32_t)decode_uleb128 ();
							if (htype > 3)
								throw new InvalidDataException("Invalid GC handle");
							mHandler.OnGCHandleDestroyed(time_base, htype, handle);
						}
						break;
					}

					case EntryType.TYPE_METADATA:
					{
						ulong tdiff = decode_uleb128 ();
						time_base += tdiff;

						bool error = (subtype & SubType.TYPE_LOAD_ERR) != 0;
						MetadataType mtype = (MetadataType)ReadByte();
						long ptrdiff = decode_sleb128 ();

						if (mtype == MetadataType.TYPE_CLASS) {
							intptr_t imptrdiff = decode_sleb128 ();
							uint64_t flags = decode_uleb128();
							if (flags!=0) {
								throw new InvalidDataException("non-zero flags in class");
							}
							if (!error) {
								var classInfo = add_class(ptr_base + ptrdiff, ReadString());
								mHandler.OnLoadClass(time_base, classInfo);
							}
						} else if (mtype == MetadataType.TYPE_IMAGE) {
							uint64_t flags = decode_uleb128 ();
							if (flags!=0) {
								throw new InvalidDataException("non-zero flags in image");
							}
							if (!error) {
								var imageInfo = add_image (ptr_base + ptrdiff, ReadString());
								mHandler.OnLoadImage(time_base, imageInfo);
							}
						} else if (mtype == MetadataType.TYPE_THREAD) {
							uint64_t flags = decode_uleb128 ();
							if (flags!=0) {
								throw new InvalidDataException("non-zero flags in thread");
							}
							ThreadDesc nt = lookup_thread (ptr_base + ptrdiff);
							nt.Name = ReadString();

//							Console.WriteLine("loaded thread {0}", nt.Name);
						}
						break;
					}
					case EntryType.TYPE_HEAP: {
						if (subtype == SubType.TYPE_HEAP_OBJECT) {
							HeapObjectDesc ho;
							int i;
							intptr_t objdiff = decode_sleb128 ();
							intptr_t ptrdiff = decode_sleb128 ();
							uint64_t size = decode_uleb128 ();
							uintptr_t num = decode_uleb128 ();
//							uintptr_t ref_offset;
							uintptr_t last_obj_offset = 0;
//							ClassDesc cd = lookup_class (ptr_base + ptrdiff);
							if (size!=0) {
//								HeapClassDesc *hcd = add_heap_shot_class (thread->current_heap_shot, cd, size);
//								if (collect_traces) {
//									ho = alloc_heap_obj (OBJ_ADDR (objdiff), hcd, num);
//									add_heap_shot_obj (thread->current_heap_shot, ho);
//									ref_offset = 0;
//								}
							} else {
//								if (collect_traces)
//									ho = heap_shot_obj_add_refs (thread->current_heap_shot, OBJ_ADDR (objdiff), num, &ref_offset);
							}
							for (i = 0; i < (int)num; ++i) {
								/* FIXME: use object distance to measure how good
								 * the GC is at keeping related objects close
								 */
								uintptr_t offset = 0;
								if (data_version > 1) 
									offset = last_obj_offset + decode_uleb128 ();
								intptr_t obj1diff = decode_sleb128 ();
								last_obj_offset = offset;
//								if (collect_traces)
//									ho->refs [ref_offset + i] = OBJ_ADDR (obj1diff);
//								if (num_tracked_objects)
//									track_obj_reference (OBJ_ADDR (obj1diff), OBJ_ADDR (objdiff), cd);
							}
//							if (debug && size)
//								fprintf (outfile, "traced object %p, size %llu (%s), refs: %d\n", (void*)OBJ_ADDR (objdiff), size, cd->name, num);
						} else if (subtype == SubType.TYPE_HEAP_ROOT) {
							uintptr_t num = decode_uleb128 ();
							uintptr_t gc_num = decode_uleb128 ();
							int i;
							for (i = 0; i < (int)num; ++i) {
								intptr_t objdiff = decode_sleb128 ();
								int root_type = (int)decode_uleb128 ();
								// we just discard the extra info for now 
								uintptr_t extra_info = decode_uleb128 ();

//								Console.WriteLine("object {0} is a {1} root", OBJ_ADDR(objdiff), root_type);

//								if (debug)
//									fprintf (outfile, "object %p is a %s root\n", (void*)OBJ_ADDR (objdiff), get_root_name (root_type));
//								if (collect_traces)
//									thread_add_root (thread, OBJ_ADDR (objdiff), root_type, extra_info);
							}
						} else if (subtype == SubType.TYPE_HEAP_END) {
							uint64_t tdiff = decode_uleb128 ();
							time_base += tdiff;
//							if (debug)
//								fprintf (outfile, "heap shot end\n");
//							if (collect_traces) {
//								HeapShot *hs = thread->current_heap_shot;
//								if (hs && thread->num_roots) {
//									/* transfer the root ownershipt to the heapshot */
//									hs->num_roots = thread->num_roots;
//									hs->roots = thread->roots;
//									hs->roots_extra = thread->roots_extra;
//									hs->roots_types = thread->roots_types;
//								} else {
//									free (thread->roots);
//									free (thread->roots_extra);
//									free (thread->roots_types);
//								}
//								thread->num_roots = 0;
//								thread->size_roots = 0;
//								thread->roots = NULL;
//								thread->roots_extra = NULL;
//								thread->roots_types = NULL;
//								heap_shot_resolve_reverse_refs (hs);
//								heap_shot_mark_objects (hs);
//								heap_shot_free_objects (hs);
//							}
//							thread->current_heap_shot = NULL;
						} else if (subtype == SubType.TYPE_HEAP_START) {
							uint64_t tdiff = decode_uleb128 ();
							time_base += tdiff;
//							if (debug)
//								fprintf (outfile, "heap shot start\n");
//							thread->current_heap_shot = new_heap_shot (time_base);
						}
						break;
					}
					case EntryType.TYPE_MONITOR: {
						uint64_t tdiff = decode_uleb128 ();
						time_base += tdiff;

						MonitorEvent mevent = (MonitorEvent)((code >> 4) & 0x3);
						intptr_t objdiff = decode_sleb128 ();
						MonitorDesc mdesc = lookup_monitor (OBJ_ADDR (obj_base, objdiff));

						MethodDesc[] backTrace = null;
						if ((subtype & SubType.TYPE_MONITOR_BT) != 0) {
							backTrace = decode_bt(ptr_base);
						}

						if (mevent == MonitorEvent.MONO_PROFILER_MONITOR_CONTENTION) {
							mHandler.OnMonitorContention( time_base, mdesc, backTrace);
						} else if (mevent == MonitorEvent.MONO_PROFILER_MONITOR_FAIL) {
							mHandler.OnMonitorFail( time_base, mdesc);
						} else if (mevent == MonitorEvent.MONO_PROFILER_MONITOR_DONE) {
							mHandler.OnMonitorDone( time_base, mdesc);
						}
						break;
					}
					case EntryType.TYPE_EXCEPTION: 
					{
						uint64_t tdiff = decode_uleb128 ();
						time_base += tdiff;

						SubType exsubtype = (SubType)(code & 0x70);
						if (exsubtype == SubType.TYPE_CLAUSE) {
							MonoExceptionClause clause_type = (MonoExceptionClause)decode_uleb128 ();
							int clause_num = (int)decode_uleb128 ();
							int64_t ptrdiff = decode_sleb128 ();
							method_base += ptrdiff;
							mHandler.OnExceptionClause(time_base, clause_type, clause_num, lookup_method(method_base));
						} else {
							intptr_t objdiff = decode_sleb128 ();

							MethodDesc[] backTrace = null;
							if ((subtype & SubType.TYPE_EXCEPTION_BT) != 0) {
								backTrace = decode_bt(ptr_base);
							}
							mHandler.OnExceptionThrown(time_base, OBJ_ADDR(obj_base, objdiff), backTrace);
						}
						break;
					}
					default:
						throw new NotImplementedException("Unhandled entry type: " + etype);
				}

			}
		}

		public void ParseFile(string path, IEventHandler handler, bool compressed)
		{
			this.mHandler = handler;
			using (var file = File.OpenRead(path)) 
			{
				Stream inputStream = file;
				// TODO: detect compression here instead
				if (compressed) {
					inputStream = new System.IO.Compression.GZipStream(file, System.IO.Compression.CompressionMode.Decompress);
				}

				using (var br = new BinaryReader(inputStream)) 
				{
					// parse file header
					logHeader = br.ReadUInt32();
					if (logHeader != (uint)LOG_HEADER_ID) 
					{
						throw new InvalidDataException("Not the right file format");
					}

					// parse file version
					version_major = br.ReadByte();  
					version_minor = br.ReadByte();  
					data_version = br.ReadByte();  
					// check version
					if (data_version != (byte)LOG_DATA_VERSION || 
					    version_major != (byte)LOG_VERSION_MAJOR || 
					    version_minor != (byte)LOG_VERSION_MINOR)
					{
						throw new InvalidDataException("Invalid profile file version");
					}

					// parse rest of header
					pointer_size = br.ReadByte();  
					startup_time = br.ReadInt64();
					timer_overhead = br.ReadInt32();
					flags = br.ReadInt32();
					pid = br.ReadInt32();
					port = br.ReadInt32();

					// parse entire file
					while (file.Position < file.Length)
					{
						mHandler.OnProgress((ulong)file.Position, (ulong)file.Length, (double)file.Position * 100.0 / (double)file.Length);

						// parse buffer
						try {
							ParseBuffer(br);
						} catch {
							return;
						}
					}
				}
			}
		}
	}
}

