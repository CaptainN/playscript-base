using System;

namespace Mono.Profiler
{
	/// <summary>
	/// Abstract base class implementation of IEventHandler
	/// </summary>
	public abstract class EventHandlerBase : IEventHandler
	{
		public ThreadDesc Thread { get; set;}

		#region IEventHandler implementation
		public virtual void OnProgress(ulong position, ulong length, double percent)
		{
		}

		public virtual void SetThread(ThreadDesc thread)
		{
			// set current thread
			this.Thread = thread;
		}

		public virtual void OnMethodEnter(ulong time, MethodDesc method)
		{
		}

		public virtual void OnMethodLeave(ulong time, MethodDesc method)
		{
		}

		public virtual void OnLoadImage(ulong time, ImageDesc desc)
		{
		}

		public virtual void OnLoadClass(ulong time, ClassDesc desc)
		{
		}

		public virtual void OnMethodCompile(ulong time, MethodDesc desc)
		{
		}

		public virtual void OnObjectAlloc(ulong time, ClassDesc classDesc, long address, ulong size, MethodDesc[] backTrace)
		{
		}

		public virtual void OnObjectMove(ulong time, long sourceAddress, long destAddress)
		{
		}

		public virtual void OnMonitorContention(ulong time, MonitorDesc desc, MethodDesc[] backTrace)
		{
		}

		public virtual void OnMonitorFail(ulong time, MonitorDesc desc)
		{
		}

		public virtual void OnMonitorDone(ulong time, MonitorDesc desc)
		{
		}

		public virtual void OnGCResize(ulong time, ulong newHeapSize)
		{
		}

		public virtual void OnGCEvent(ulong time, int generation, MonoGCEvent eventType)
		{
		}

		public virtual void OnGCHandleCreated(ulong time, int handleType, uint handle, long address)
		{
		}

		public virtual void OnGCHandleDestroyed(ulong time, int handleType, uint handle)
		{
		}

		public virtual void OnExceptionThrown(ulong time, long exceptionObject, MethodDesc[] backTrace)
		{
		}

		public virtual void OnExceptionClause(ulong time, MonoExceptionClause type, int num, MethodDesc method)
		{
		}

		#endregion
	}
}

