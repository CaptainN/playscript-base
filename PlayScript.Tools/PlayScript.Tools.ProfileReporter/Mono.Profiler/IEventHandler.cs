using System;

namespace Mono.Profiler
{
	public class Descriptor
	{
		// can store tracking information here
		public object UserData; 
		// name of this descriptor
		public string Name;

		public override string ToString() {
			return Name;
		}
	};

	public class ClassDesc : Descriptor
	{
	};

	public class ImageDesc : Descriptor
	{
	};

	public class MethodDesc : Descriptor
	{
		public long     		Code;
		public int 				Length;
		public Action<ulong,MethodDesc>	OnEnter;
		public Action<ulong,MethodDesc>	OnLeave;
	};

	public class ThreadDesc : Descriptor
	{
		public int      Id;
	};

	public class MonitorDesc : Descriptor
	{
	};

	public class HeapObjectDesc : Descriptor
	{
	};

	public enum MonoGCEvent 
	{
		MONO_GC_EVENT_START,
		MONO_GC_EVENT_MARK_START,
		MONO_GC_EVENT_MARK_END,
		MONO_GC_EVENT_RECLAIM_START,
		MONO_GC_EVENT_RECLAIM_END,
		MONO_GC_EVENT_END,
		MONO_GC_EVENT_PRE_STOP_WORLD,
		MONO_GC_EVENT_POST_STOP_WORLD,
		MONO_GC_EVENT_PRE_START_WORLD,
		MONO_GC_EVENT_POST_START_WORLD
	};

	public enum MonoProfileGCRootType
	{
		MONO_PROFILE_GC_ROOT_STACK = 0,
		MONO_PROFILE_GC_ROOT_FINALIZER = 1,
		MONO_PROFILE_GC_ROOT_HANDLE = 2,
		MONO_PROFILE_GC_ROOT_OTHER = 3,
		MONO_PROFILE_GC_ROOT_MISC = 4, /* could be stack, handle, etc. */
	};

	[Flags]
	public enum MonoProfileGCRootFlags
	{
		MONO_PROFILE_GC_ROOT_PINNING  = 1 << 8,
		MONO_PROFILE_GC_ROOT_WEAKREF  = 2 << 8,
		MONO_PROFILE_GC_ROOT_INTERIOR = 4 << 8,
	};

	public enum MonoExceptionClause 
	{
		MONO_EXCEPTION_CLAUSE_NONE,
		MONO_EXCEPTION_CLAUSE_FILTER,
		MONO_EXCEPTION_CLAUSE_FINALLY,
		MONO_EXCEPTION_CLAUSE_FAULT = 4
	};


	public interface IEventHandler
	{
		/// <summary>
		/// Called periodically to update the progress of the file parsing
		/// </summary>
		/// <param name="position">Position in file</param>
		/// <param name="length">Length of file</param>
		/// <param name="percent">Percent complete</param>
		void OnProgress(ulong position, ulong length, double percent);

		/// <summary>
		/// Sets the current thread of execution. All method/allocs are done on this current thread.
		/// </summary>
		/// <param name="thread">Thread.</param>
		void SetThread(ThreadDesc thread);

		//
		// metadata callbacks
		//

		/// <summary>
		/// Called when an image (executable) is loaded
		/// </summary>
		void OnLoadImage(ulong time, ImageDesc desc);

		/// <summary>
		/// Called when an class is loaded
		/// </summary>
		void OnLoadClass(ulong time, ClassDesc desc);

		//
		// heap object callbacks
		//

		/// <summary>
		/// Called when an object is allocated (new)
		/// </summary>
		void OnObjectAlloc(ulong time, ClassDesc desc, long address, ulong size, MethodDesc[] backTrace);

		/// <summary>
		/// Called when object is moved by the garbage collector
		/// </summary>
		void OnObjectMove(ulong time, long sourceAddress, long destAddress);

		//
		// method callbacks
		//

		/// <summary>
		/// Called when an method is compiled via the JIT-er
		/// </summary>
		void OnMethodCompile(ulong time, MethodDesc method);

		/// <summary>
		/// Called when an method is entered
		/// </summary>
		void OnMethodEnter(ulong time, MethodDesc method);

		/// <summary>
		/// Called when returning from a method. There should be a leave for each OnMethodEnter
		/// </summary>
		void OnMethodLeave(ulong time, MethodDesc method);

		//
		// monitor callbacks
		//

		/// <summary>
		/// Called when a monitor lock is being acquired but it is already locked
		/// </summary>
		void OnMonitorContention(ulong time, MonitorDesc monitor, MethodDesc[] backTrace);
		/// <summary>
		/// Called when a monitor lock acquire fails
		/// </summary>
		void OnMonitorFail(ulong time, MonitorDesc monitor);
		/// <summary>
		/// Called when a monitor lock acquire completes
		/// </summary>
		void OnMonitorDone(ulong time, MonitorDesc monitor);

		//
		// GC callbacks
		//

		/// <summary>
		/// Called when the GC resizes the heap
		/// </summary>
		void OnGCResize(ulong time, ulong newHeapSize);

		/// <summary>
		/// Called when a GC starts or stops
		/// </summary>
		void OnGCEvent(ulong time, int generation, MonoGCEvent eventType);

		/// <summary>
		/// Called when a GC handle is created
		/// </summary>
		void OnGCHandleCreated(ulong time, int handleType, uint handle, long address);

		/// <summary>
		/// Called when a GC handle is destroyed
		/// </summary>
		void OnGCHandleDestroyed(ulong time, int handleType, uint handle);

		/// <summary>
		/// Called when an exception is thrown
		/// </summary>
		void OnExceptionThrown(ulong time, long exceptionObject, MethodDesc[] backTrace);

		/// <summary>
		/// Called when an exception clause is entered
		/// </summary>
		void OnExceptionClause(ulong time, MonoExceptionClause type, int num, MethodDesc method);
	};
}

