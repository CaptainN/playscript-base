using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Amf;
using PlayScript.Expando;

namespace PlayScript.Tools.AMFCodeGen
{
	class MainClass
	{

		private static string GetClassName(Amf3ClassDef classDef)
		{
			string name = classDef.Name;

			if (name[0] == '.') {
				// if starts with a dot then just uppercase it
				name = name.Substring(1, 1).ToUpper() + name.Substring(2);
			}

			return name.Replace('.', '_');
		}

		private static string GetPropertyType(object value)
		{
			if (value == null) {
				return "null";
			} else if (value is Amf3Object) {
				return GetClassName(((Amf3Object)value).ClassDef);
			} else if (value is bool) {
				return "bool";
			} else if (value is int) {
				return "int";
			} else if (value is uint) {
				return "uint";
			} else if (value is double) {
				return "double";
			} else if (value is string) {
				return "string";
			} else if (value is _root.Vector<int>){
				return "Vector<int>";
			} else if (value is _root.Vector<uint>){
				return "Vector<uint>";
			} else if (value is _root.Vector<double>){
				return "Vector<double>";
			} else if (value is _root.Vector<dynamic>){
				return "Vector<dynamic>";
			} else {
				return value.GetType().Name;
			}
		}

		private static string Quote(string str)
		{
			return "\"" + str + "\"";
		}

		private static void Indent(TextWriter tw)
		{
			// add indent
			tw.NewLine += "    ";
			// newline and indent next line
			tw.WriteLine();
		}

		private static void UnIndent(TextWriter tw)
		{
			var newline = tw.NewLine;
			// remove indent
			tw.NewLine = newline.Substring(0, newline.Length - 4);
			// newline and indent next line
			tw.WriteLine();
		}


		private static void WriteClassDef(TextWriter tw, Amf3ClassDef classDef, IList<Amf3Object> classObjects )
		{
			var propertyTypes = new Dictionary<string, string>();
			tw.WriteLine("[Amf3Serializable({0})]", Quote(classDef.Name));
			tw.WriteLine("public class {0} : IAmf3Serializable", GetClassName(classDef));
			tw.Write("{");
			Indent(tw); 

			// write all fields
			foreach (var property in classDef.Properties) {
				// find all property types of all objects we have seen
				string comment = "";
				string type = "object";

				List<string> propertyTypeList = classObjects.Select(o => GetPropertyType(o.Properties[property])).Where(o => o != "null").Distinct().ToList();
				if (propertyTypeList.Count == 0) {
					type = "object";
				} else 	if (propertyTypeList.Count == 1) {
					// only one property type found, so use it 
					type = propertyTypeList[0];
				} else {
					propertyTypeList.Sort();

					if (propertyTypeList.Count == 2 && propertyTypeList[0] == "double" && propertyTypeList[1] == "int") {
						// if they are all numeric, then use double
						type = "double";
					} else {
						// multiple property types found, so use object
						comment = "// multiple types found: ";
						propertyTypeList.ForEach(x => comment += x + " ");
					}
				}

				// write field
				tw.WriteLine("public {0} {1}; {2}", type, property, comment);
				propertyTypes.Add(property, type);
			}
			tw.WriteLine();

			tw.WriteLine("#region IAmf3Serializable implementation");

			// generate serialization writer
			tw.Write("public void Serialize(Amf3Writer writer) {");
			Indent(tw);
			tw.Write("writer.WriteObjectHeader(ClassDef);");
			foreach (var property in classDef.Properties) {
				tw.WriteLine();
				tw.Write("writer.Write({0});", property);
			}
			UnIndent(tw);
			tw.WriteLine("}");
			tw.WriteLine();

			// generate serialization reader
			tw.Write("public void Serialize(Amf3Parser reader) {");
			Indent(tw);
			foreach (var property in classDef.Properties) {
				string type = propertyTypes[property];
				tw.WriteLine();
				tw.Write("reader.Read(out {0});", property);
			}
			UnIndent(tw);
			tw.WriteLine("}");

			// end region
			tw.WriteLine("#endregion");
			tw.WriteLine();

			// write class definition
			string propNames = "{";
			bool delimiter = false;
			foreach (var property in classDef.Properties) {
				if (delimiter) propNames += ", ";
				propNames += Quote(property);
				delimiter = true;
			}
			propNames += "}";
			tw.Write("public static Amf3ClassDef ClassDef = new Amf3ClassDef({0}, new string[] {1} );",
			                            Quote(classDef.Name),
			                            propNames);

			// end class
			UnIndent(tw);
			tw.WriteLine("}");
		}

		private static void PrintVector<T>(TextWriter tw, IList<T> v, bool newline = false)
		{
			tw.Write("[");
			if (newline)
				Indent(tw);

			for (int i=0; i < v.Count; i++) {
				if (i > 0)
				{
					tw.Write(',');
					if (newline)
						tw.WriteLine();
				}
				PrintObject(tw, v[i]);
			}
			if (newline)
				UnIndent(tw);
			tw.Write("]");
		}

		private static void PrintObject(TextWriter tw, object o)
		{
			if (o == null) {
				tw.Write("null");
			} else if (o is string) {
				tw.Write("\"{0}\"", ((string)o));
			} else if (o is Amf3Object) {
				var ao = (Amf3Object)o; 
				tw.Write("({0}) {{", ao.ClassDef.Name);
				Indent(tw);
				bool delimiter = false;
				foreach (var prop in ao.Properties) {
					if (delimiter) tw.WriteLine(); else delimiter = true;
					tw.Write("{0}: ", prop.Key);
					PrintObject(tw, prop.Value);
				}
				UnIndent(tw);
				tw.Write("}");
			} else if (o is _root.Array){
				tw.Write("(Array)");
				PrintVector(tw, (o as _root.Array)._GetInnerArray(), true);
			} else if (o is _root.Vector<int>){
				tw.Write("(int)");
				PrintVector(tw, o as _root.Vector<int>);
			} else if (o is _root.Vector<uint>){
				tw.Write("(uint)");
				PrintVector(tw, o as _root.Vector<uint>);
			} else if (o is _root.Vector<double>){
				tw.Write("(number)");
				PrintVector(tw, o as _root.Vector<double>);
			} else if (o is _root.Vector<dynamic>){
				tw.Write("(dynamic)");
				PrintVector(tw, o as _root.Vector<dynamic>, true);
			} else if (o is flash.utils.Dictionary){
				var d = o as flash.utils.Dictionary;
				tw.Write("({0}) {{", "dictionary");
				Indent(tw);
				bool delimiter = false;
				foreach (var prop in d) {
					if (delimiter) tw.WriteLine(); else delimiter = true;
					PrintObject(tw, (object)prop.Key);
					tw.Write(": ");
					PrintObject(tw, (object)prop.Value);
				}
				UnIndent(tw);
				tw.WriteLine("}");
			} else if (o is double){
				tw.Write("(number){0}", o);
			} else {
				// ?? 
				tw.Write(o.ToString());
			}
		}

		public static void DumpAllObjects(Stream stream, TextWriter tw)
		{
			Amf3Parser parser = new Amf3Parser(stream);

			// parse entire file
			while (stream.Position < stream.Length) {
				var o = parser.ReadNextObject();
				PrintObject(tw, o);
				if (o == null)
					break;
			}
		}

		public static void CopyAllObjects(Stream inStream, Stream outStream)
		{
			Amf3Parser parser = new Amf3Parser(inStream);
			Amf3Writer writer = new Amf3Writer(outStream);

			// parse entire file
			while (inStream.Position < inStream.Length) {
				// read object
				var o = parser.ReadNextObject();
				// write object
				writer.Write(o);
			}
		}



		public static void DumpAllClassDefs(string sourceFileName, Stream stream, TextWriter tw, string namespaceName, string className)
		{
			Amf3Parser parser = new Amf3Parser(stream);

			// parse entire file
			while (stream.Position < stream.Length) {
				var o = parser.ReadNextObject();
				if (o == null)
					break;
			}

			var classDefList = parser.GetClassDefinitions();
			var objectTable = parser.GetObjectTable();
			// select only the Amf3Objects
			var amfObjectList = objectTable.Where(o => o is Amf3Object).Select(o => (Amf3Object)o).ToList();

			var hasWritten = new Dictionary<string, bool>();

			tw.WriteLine("// This file was autogenerated from {0}", sourceFileName);
			var tool = System.IO.Path.GetFileNameWithoutExtension(System.Environment.GetCommandLineArgs()[0]);
			tw.WriteLine("// Using the tool {0} from the PlayScript project", tool);

			if (namespaceName != null)
			{
				tw.WriteLine("using System;");
				tw.WriteLine("using _root;");
				tw.WriteLine("using flash.utils;");
				tw.WriteLine("using Amf;");
				tw.WriteLine("");
				tw.Write("namespace {0} {{", namespaceName);
				Indent(tw);
			}

			if (className != null) {
				tw.Write("public static class {0} {{", className);
				Indent(tw);
			}

			// write all class defintions
			foreach (var classDef in classDefList) {
				if (!hasWritten.ContainsKey(classDef.Name)) {
					// get all objects of this class
					var classObjects = amfObjectList.Where(o => o.ClassDef == classDef).ToList();
					WriteClassDef(tw, classDef, classObjects);
					tw.WriteLine();
					hasWritten.Add(classDef.Name, true);
				}
			}

			if (className != null) {
				UnIndent(tw);
				tw.WriteLine("} // end class");
			}
			if (namespaceName != null) {
				UnIndent(tw);
				tw.WriteLine("} // end namespace");
			}
		}


		class ExpandoInfo
		{
			public string         parentName;
			public ExpandoObject  expando;
			public string[]		  properties;
			public string 		  propertyHash;
		}

		private static void GetAllExpandoObjects(object root, string parentName, IList<ExpandoInfo> list)
		{
			if (root is ExpandoObject) {
				var expando = (ExpandoObject)root;

				var info = new ExpandoInfo();
				info.parentName = parentName;
				info.properties = GetExpandoProperties(expando);
				info.propertyHash = GetPropertyHash(info.properties);
				info.expando      = expando;
				list.Add(info);

				foreach (var kvp in expando) {
					GetAllExpandoObjects(kvp.Value, kvp.Key, list);
				}
			} else if (root is System.Collections.IList) {
				var array = (System.Collections.IList)root;
				foreach (var element in array) {
					GetAllExpandoObjects(element, parentName, list);
				}
			} else {
				// value type? ignore?
			}
		}

		private static string GetPropertyHash(string[] properties)
		{
			var sb = new System.Text.StringBuilder();
			foreach (var prop in properties) {
				sb.Append(prop);
				sb.Append('|');
			}
			return sb.ToString();
		}

		private static string[] GetExpandoProperties(ExpandoObject expando)
		{
			var list = new List<string>();
			foreach (var kvp in expando) {
				list.Add(kvp.Key);
			}
			list.Sort();
			return list.ToArray();
		}

		private static string GetExpandoPropertyType(object o)
		{
			if (o is bool) {
				return "bool";
			}
			if (o == null) {
				return "null";
			}
			if (o is double) {
				return "double";
			}
			if (o is string) {
				return "string";
			}
			if (o is ExpandoObject) {
				return "object";
			}
			if (o is _root.Array) {
				return "Array";
			}
			if (o is int) {
				return "int";
			}
			if (o is uint) {
				return "uint";
			}
			return o.GetType().ToString();
		}

		private static void JsonToAmfOld(StreamReader sr, Stream outStream)
		{
			// read json text
			string jsonText = sr.ReadToEnd();

			// parse json
			object root = _root.JSON.parse(jsonText);

			var list = new List<ExpandoInfo>();
			GetAllExpandoObjects(root, "", list);

			var groups =
				from info in list
				group info by info.propertyHash into g
				select new {key = g.Key, list = g};

//			var dict = new Dictionary<ExpandoObject, Amf3ClassDef>();

			foreach (var group in groups) {
				List<ExpandoInfo> infos = group.list.ToList();

//				Console.WriteLine("{0} count:{1}", group.key, infos.Count);

				string className = "";

				var parentNames = infos.Select(o => o.parentName).Distinct().ToList();

				if (parentNames.Count == 1 && !string.IsNullOrEmpty(parentNames[0])) {
					className += "_" + parentNames[0];
				}

				var properties = new List< Tuple<string, string> >();

				ExpandoInfo first = infos.First();
				foreach (string prop in first.properties) {

					var propertyValues = infos.Select(o => o.expando[prop])
										.Distinct()
										.ToList();


					// get unique property value types
					var propertyTypes = propertyValues
											.Select(o=>GetExpandoPropertyType(o))
											.Where(o => o!="null")
											.ToList()
											.Distinct()
											.ToList();

					if (propertyTypes.Count == 1) {
						if (propertyValues.Count == 1 && propertyTypes[0] == "string") {
							className += "_" + propertyValues[0];
						} else {
							// add property to class
							properties.Add(new Tuple<string, string>(prop, propertyTypes[0]));
						}
					} else {
						// add property to class
						properties.Add(new Tuple<string, string>(prop, "object"));
					}

//					Console.Write("{0}:", prop);
//					foreach (var pt in propertyTypes) {
//						Console.Write(pt.ToString());
//					}
//					Console.Write(" -- ");
//					foreach (var pv in propertyValues) {
//						Console.Write(pv.ToString());
//					}
//					Console.WriteLine();
				}

				if (!string.IsNullOrEmpty(className))
				{
					Console.WriteLine("public class {0} {{ // count:{1}", className, infos.Count);
					var propNames = new List<string>();
					foreach (var prop in properties) {
						Console.WriteLine("    public {1,-8} {0}; ", prop.Item1, prop.Item2);
						propNames.Add(prop.Item1);
					}
					Console.WriteLine("}");
					Console.WriteLine();

					var classDef = new Amf3ClassDef(className, propNames.ToArray(), false, false);
					foreach (var info in infos) {
						info.expando.ClassDefinition = classDef;
					}
				}
			}

			// write to amf stream
			var amfWriter = new Amf3Writer(outStream);
			amfWriter.Write(root);
		}

		// this function generates AMF3 class definitions for all dynamic objects it finds
		// having shared class definitions will result in a much more compact serialized form
		private static void GenerateClassDefinitions(object root, IList<ExpandoObject> expandos, Dictionary<string, Amf3ClassDef> classDefs)
		{
			if (root is ExpandoObject) {
				var expando = (ExpandoObject)root;
				expandos.Add(expando);

				// get properties from expando
				var properties = new List<string>();
				foreach (var kvp in expando) {
					properties.Add(kvp.Key);
				}
				// sort them
				properties.Sort();

				// hash them as one string
				var sb = new System.Text.StringBuilder();
				foreach (var prop in properties) {
					sb.Append(prop);
					sb.Append('|');
				}

				var propertyHash = sb.ToString();

				Amf3ClassDef classDef;
				// see if a class definition already exists for these properties...
				if (!classDefs.TryGetValue(propertyHash, out classDef)) {
					// does not exist, so create it and add it to dictionary
					classDef = new Amf3ClassDef("*", properties.ToArray());
					classDefs.Add(propertyHash, classDef);
				}

				// set class definition for expando object
				expando.ClassDefinition = classDef;

				// recurse expando properties
				foreach (var kvp in expando) {
					GenerateClassDefinitions(kvp.Value, expandos, classDefs);
				}
			} else if (root is System.Collections.IList) {
				var array = (System.Collections.IList)root;
				foreach (var element in array) {
					GenerateClassDefinitions(element, expandos, classDefs);
				}
			} 
		}



		private static void JsonToAmf(StreamReader sr, Stream outStream)
		{
			// read json text
			string jsonText = sr.ReadToEnd();

			// parse json
			object root = _root.JSON.parse(jsonText);

			// generate class definitions for all expandos
			var expandos = new List<ExpandoObject>();
			var classDefs = new Dictionary<string, Amf3ClassDef>();
			GenerateClassDefinitions(root, expandos, classDefs);
			Console.WriteLine("expandos: {0} classDefs: {1}", expandos.Count, classDefs.Count);

			// write to amf stream
			var amfWriter = new Amf3Writer(outStream);
			amfWriter.Write(root);
		}


		private static void PrintUsage()
		{
			Console.WriteLine("AMF utility");
			Console.WriteLine("Usage: \n" +
			                  "  Generate runtime classes from AMF class definitions: \n" +
			                  "     AMFCodeGen --class <amf-file> [<output-file>]\n" +
			                  "  Parse AMF stream and print all objects: \n" +
			                  "     AMFCodeGen --objects <amf-file> [<output-file>]\n" +
			                  "  Copy AMF stream from one file to another: \n" +
			                  "     AMFCodeGen --copy <amf-file> [<output-file>]\n" +
			                  "  Optimize AMF stream: \n" +
			                  "     AMFCodeGen --optimize <amf-file> [<output-file>]\n"+
				    		  "  Convert JSON to AMF: \n" +
							  "     AMFCodeGen --json <json-file> [<amf-file>]\n");
		}

		public static void Main(string[] args)
		{
			if (args.Length == 0) {
				PrintUsage();
				return;
			}

			int i = 0;

			// read command and options
			string command = null;
			var options = new Dictionary<string, object>();
			while (i < args.Length && args[i][0] == '-') {
				if (i == 0) 
					command = args[i];
				else
					options.Add(args[i], true);
				i++;
			}

			if (command == null) {
				PrintUsage();
				return;
			}

			// read files
			var files = new List<string>();
			while (i < args.Length) {
				files.Add(args[i++]);
			}

			if (files.Count == 0) {
				PrintUsage();
				return;
			}

			if (files.Count == 1) {
				switch (command) {
					case "--class":
						files.Add(files[0] + ".cs");
						break;
					case "--object":
						files.Add(files[0] + ".object");
						break;
					case "--copy":
						files.Add(files[0] + ".copy");
						break;
					case "--optimize":
						files.Add(files[0] + ".optimized");
						break;
					case "--json":
						files.Add(files[0] + ".amf");
						break;
					default:
						PrintUsage();
						return;
				}
			}



			string sourceFile = files[0];
			string destFile = files[1];

			// todo:
			string namespaceName = null;
			string className = null;

			try {
				switch (command) {
					case "--class":
						using (var fs = File.OpenRead(sourceFile)) {
							using (var tw = new StreamWriter(destFile)) {
								DumpAllClassDefs(sourceFile, fs, tw, namespaceName, className);
							}
						}
					break;
					case "--object":
						using (var fs = File.OpenRead(sourceFile)) {
							using (var tw = new StreamWriter(destFile)) {
								DumpAllObjects(fs, tw);
							}
						}
						break;
					case "--copy":
						using (var fs = File.OpenRead(sourceFile)) {
							using (var fd = File.Create(destFile)) {
								CopyAllObjects(fs, fd);
							}
						}
						break;
					case "--json":
						using (var fs = new StreamReader(sourceFile)) {
							using (var fd = File.Create(destFile)) {
								JsonToAmf(fs, fd);
							}
						}
						break;
					default:
						PrintUsage();
						return;
				}

				Console.WriteLine("AMFCodeGen {0} {1} to {2}", command, sourceFile, destFile);
			}
			catch (Exception e) {
				Console.WriteLine("Error: {0}", e.ToString());
			}
		}
	}
}
