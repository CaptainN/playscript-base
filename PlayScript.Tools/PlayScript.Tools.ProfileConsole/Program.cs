using System;
using System.Collections.Generic;
using System.Diagnostics;

using PlayScript.Tools.ProfileCore;

namespace PlayScript.Tools.ProfileConsole
{
	public static class Program
	{
		class XamarinShortcomingListener : TraceListener
		{
			public override void Write (string message)
			{
				Debugger.Break();
			}

			public override void WriteLine (string message)
			{
				Debugger.Break();
			}
		}

		static int Main(string[] args)
		{
			// So break actually breaks in the debugger!
			Trace.Listeners.Add(new XamarinShortcomingListener());

			string[] fileNames =
			{
#if false
				"/Users/onallet/Desktop/profiler-output/frame_perf#2082.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#1739.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#1318.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#1434.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#820.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#1901.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#1495.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#16.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#1633.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#72.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#42.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#2061.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#2083.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#1775.txt",
				"/Users/onallet/Desktop/profiler-output/frame_perf#2084.txt",
#else
				"/Users/onallet/Desktop/profiler-output/frame_perf#114.txt",
#endif
			};
			ProfileInfo topProfileInfo = ProfileCore.TextFormat.ProfileReader.ReadFiles(fileNames);

			//PrintProfiles(topProfileInfo);

			// This does not work very well as there is too many profile info duplication going on
			// and the code is running out of memory quickly for several frames.
			//MergeAndPrintHotSpots(10, 5);

			IProfileComparer comparer = new AllocatedBytesComparer();
			MergeByCallstacksAndPrintHotSpots(topProfileInfo, comparer, 100, 10, 100, 10);

			return 0;
		}

		static void PrintProfiles(ProfileInfo topProfileInfo)
		{
			ProfileInfoVisitor.Visit(topProfileInfo, new ConsoleVisitor());
		}

		static void MergeByProfileAndPrintHotSpots(ProfileInfo topProfileInfo, int numberOfProfileInfosToPrint, int numberOfCallersToPrint)
		{
			Console.WriteLine("Merge methods");
			MergerVisitor mergerVisitor = new MergerVisitor();
			ProfileInfoVisitor.Visit(topProfileInfo, mergerVisitor);

			Console.WriteLine("Sort by number of calls");
			ProfileInfo[] mergedProfileInfos = mergerVisitor.MergedProfileInfos;
			ProfileInfo.SortByNumberOfCalls(mergedProfileInfos);

			int numberOfProfileInfos = 0;
			foreach (ProfileInfo profileInfo in mergedProfileInfos)
			{
				Console.WriteLine("{0} times: {1}", profileInfo.NumberOfCalls, profileInfo.Name);

				ProfileInfo[] callers = profileInfo.Callers;
				callers = ProfileInfo.MergeSame(callers);
				ProfileInfo.SortByNumberOfCalls(callers);
				int numberOfCallers = 0;
				foreach (ProfileInfo caller in callers)
				{
					Console.WriteLine("    Called {0} times by: {1}", caller.NumberOfCalls, caller.Name);
					if (++numberOfCallers > numberOfCallersToPrint)
					{
						break;
					}
				}

				if (++numberOfProfileInfos >= numberOfProfileInfosToPrint)
				{
					break;
				}
			}
		}

		static void MergeByCallstacksAndPrintHotSpots(ProfileInfo topProfileInfo, IProfileComparer comparer, int numberOfMethodsToPrint, int numberOfCallstacksByMethod, int numberOfCallstackToPrint, int numberOfMethodsInCallstack)
		{
			Console.WriteLine("Gather callstacks");
			CallstackVisitor callstackVisitor = new CallstackVisitor();
			ProfileInfoVisitor.Visit(topProfileInfo, callstackVisitor);

			// Sort all callstack by usage
			var allCallstacks = callstackVisitor.CallstackUsages.Values;
			CallstackUsage[] sortedCallstackUsages = new CallstackUsage[allCallstacks.Count];
			allCallstacks.CopyTo(sortedCallstackUsages, 0);
			Array.Sort(sortedCallstackUsages, (IComparer<CallstackUsage>)comparer);

			//Sort method name by usage
			var allMethodUsages = callstackVisitor.NamesToMethodUsages.Values;
			var sortedMethodUsages = new MethodUsage[allMethodUsages.Count];
			allMethodUsages.CopyTo(sortedMethodUsages, 0);
			Array.Sort(sortedMethodUsages, (IComparer<MethodUsage>)comparer);

			int numberOfMethods = 0;
			Console.WriteLine("Top methods (and corresponding callstacks) by {0}:", comparer.Name);
			foreach (MethodUsage methodUsage in sortedMethodUsages)
			{
				comparer.PrintMethodUsage(methodUsage);

				Callstack[] sortedCallstacks = new Callstack[methodUsage.Callstacks.Count];
				methodUsage.Callstacks.CopyTo(sortedCallstacks, 0);
				Array.Sort(sortedCallstacks, (IComparer<Callstack>)comparer);
				int numberOfCallstacksForMethod = 0;
				foreach (Callstack callstack in sortedCallstacks)
				{
					comparer.PrintCallstack(callstack, numberOfCallstacksForMethod, numberOfMethodsInCallstack, "    ");
					if (++numberOfCallstacksForMethod > numberOfCallstacksByMethod)
					{
						break;
					}
				}

				if (++numberOfMethods > numberOfMethodsToPrint)
				{
					break;
				}
			}

			Console.WriteLine("");
			Console.WriteLine("Top callstacks:");
			int numberOfCallstacks = 0;
			foreach (CallstackUsage callstackUsage in sortedCallstackUsages)
			{
				comparer.PrintCallstack(callstackUsage.Callstack, numberOfCallstacks, numberOfMethodsInCallstack);
				if (++numberOfCallstacks > numberOfCallstackToPrint)
				{
					break;
				}
			}
		}

		class MergerVisitor : BaseVisitor
		{
			Dictionary<string, ProfileInfo> mMergedProfileInfos = new Dictionary<string, ProfileInfo>();

			public ProfileInfo[]	MergedProfileInfos
			{
				get
				{
					return ProfileInfo.ToArray(mMergedProfileInfos.Values);
				}
			}

			public override void OnEnterMethod(ProfileInfo profileInfo)
			{
				ProfileInfo foundMatchingProfile;
				if (mMergedProfileInfos.TryGetValue(profileInfo.Name, out foundMatchingProfile) == false)
				{
					foundMatchingProfile = new ProfileInfo(profileInfo.Name);
					mMergedProfileInfos.Add(profileInfo.Name, foundMatchingProfile);
				}

				// Now let's merge the content
				foundMatchingProfile.Merge(profileInfo);
			}
		}

		class ConsoleVisitor : BaseVisitor
		{
			string indent = "";

			public override void OnEnterMethod(ProfileInfo profileInfo)
			{
				Console.WriteLine(indent + "enter " + profileInfo);
				indent += " ";
			}

			public override void OnLeaveMethod(ProfileInfo profileInfo)
			{
				if (indent.Length > 0)
				{
					indent = indent.Substring(1);
				}
				Console.WriteLine(indent + "leave " + profileInfo);
			}
		}

		public class CallstackUsage
		{
			public CallstackUsage(Callstack callstack)
			{
				Callstack = callstack;
			}
			public Callstack			Callstack { get; private set; }
			// We store the usage in the callstack so we can sort callstacks between them
			//public int					Usage;
		}

		public class MethodUsage
		{
			public MethodUsage(string name)
			{
				Name = name;
			}
			public string				Name { get; private set; }
			public HashSet<Callstack>	Callstacks = new HashSet<Callstack>();
			public int					Usage;
			public ulong				TimeSpentExclusively;
			public ulong				TimeSpentInclusively;
			public ulong				AllocatedBytes;
			public int					NumberOfAllocations;
		}

		class CallstackVisitor: BaseVisitor
		{
			public Dictionary<Callstack, CallstackUsage> CallstackUsages = new Dictionary<Callstack, CallstackUsage>();

			public Dictionary<string, MethodUsage> NamesToMethodUsages = new Dictionary<string, MethodUsage>();

			public override void OnEnterMethod(ProfileInfo profileInfo)
			{
				if (profileInfo.IsFrame)
				{
					return;
				}
				// Match a callstack to that particular profile info, update the usage (so we know how many callstacks are similar)
				Callstack callstack = new Callstack(profileInfo);
				CallstackUsage callstackUsage;
				if (CallstackUsages.TryGetValue(callstack, out callstackUsage) == false)
				{
					callstackUsage = new CallstackUsage(callstack);
					CallstackUsages.Add(callstack, callstackUsage);
				}
				callstackUsage.Callstack.Usage++;
				callstackUsage.Callstack.TimeSpentExclusively += profileInfo.TimeSpentExclusively;
				callstackUsage.Callstack.TimeSpentInclusively += profileInfo.TimeSpentInclusively;
				if (profileInfo.Allocations != null)
				{
					callstackUsage.Callstack.NumberOfAllocations += profileInfo.Allocations.Count;
					foreach (AllocInfo alloc in profileInfo.Allocations)
					{
						callstackUsage.Callstack.AllocatedBytes += (ulong)alloc.AllocatedSize;	// We discard the type allocated here
																								// We could keep the info around though, if we wanted.
					}
				}

				// While we are at it, let's map a given function to a list of callstack to reach it
				// So we have two critical informations (what callstack is hot, what are the main callstack we should optimize for a particular function)

				string name = profileInfo.Name;
				MethodUsage methodUsage;
				if (NamesToMethodUsages.TryGetValue(name, out methodUsage) == false)
				{
					methodUsage = new MethodUsage(name);
					NamesToMethodUsages.Add(name, methodUsage);
				}
				if (methodUsage.Callstacks.Contains(callstack) == false)
				{
					methodUsage.Callstacks.Add(callstack);
				}
				methodUsage.Usage++;
				methodUsage.TimeSpentExclusively += profileInfo.TimeSpentExclusively;
				methodUsage.TimeSpentInclusively += profileInfo.TimeSpentInclusively;
				if (profileInfo.Allocations != null)
				{
					methodUsage.NumberOfAllocations += profileInfo.Allocations.Count;
					foreach (AllocInfo alloc in profileInfo.Allocations)
					{
						methodUsage.AllocatedBytes += (ulong)alloc.AllocatedSize;	// We discard the type allocated here
																					// We could keep the info around though, if we wanted.
					}
				}
			}
		}

		interface IProfileComparer : IComparer<CallstackUsage>, IComparer<MethodUsage>, IComparer<Callstack>
		{
			string Name { get; }
			void PrintMethodUsage(MethodUsage methodUsage);
			void PrintCallstack(Callstack callstack, int callstackIndex, int numberOfMethods, string indentation = "");
		}

		class UsageComparer : IProfileComparer
		{
			public string Name { get { return "usage"; } }

			public void PrintMethodUsage(MethodUsage methodUsage)
			{
				Console.WriteLine("Called {0} times: {1}", methodUsage.Usage, methodUsage.Name);
			}

			public void PrintCallstack(Callstack callstack, int callstackIndex, int numberOfMethods, string indentation)
			{
				Console.WriteLine("Callstack #{0} called {1} times.", callstackIndex, callstack.Usage);
				Console.WriteLine(callstack.ToString(numberOfMethods, indentation + "    "));
			}

			public int Compare (CallstackUsage x, CallstackUsage y)
			{
				return y.Callstack.Usage - x.Callstack.Usage;
			}

			public int Compare (MethodUsage x, MethodUsage y)
			{
				return y.Usage - x.Usage;
			}

			public int Compare (Callstack x, Callstack y)
			{
				return y.Usage - x.Usage;
			}
		}

		class ComparerBase
		{
			protected static int CompareUlong(ulong left, ulong right)
			{
				if (left > right)
				{
					return 1;
				}
				else if (left < right)
				{
					return -1;
				}
				else
				{
					return 0;
				}
			}
		}

		class ExclusiveTimeComparer : ComparerBase, IProfileComparer
		{
			public string Name { get { return "exclusive time"; } }

			public void PrintMethodUsage(MethodUsage methodUsage)
			{
				Console.WriteLine("Exclusive ns time: {0} - {1}", methodUsage.TimeSpentExclusively, methodUsage.Name);
			}

			public void PrintCallstack(Callstack callstack, int callstackIndex, int numberOfMethods, string indentation)
			{
				Console.WriteLine("Callstack #{0} has exclusive ns time: {1}.", callstackIndex, callstack.TimeSpentExclusively);
				Console.WriteLine(callstack.ToString(numberOfMethods, indentation + "    "));
			}

			public int Compare (CallstackUsage x, CallstackUsage y)
			{
				return CompareUlong(y.Callstack.TimeSpentExclusively, x.Callstack.TimeSpentExclusively);
			}

			public int Compare (MethodUsage x, MethodUsage y)
			{
				return CompareUlong(y.TimeSpentExclusively, x.TimeSpentExclusively);
			}

			public int Compare (Callstack x, Callstack y)
			{
				return CompareUlong(y.TimeSpentExclusively, x.TimeSpentExclusively);
			}
		}

		class AllocatedBytesComparer : ComparerBase, IProfileComparer
		{
			public string Name { get { return "memory allocated"; } }

			public void PrintMethodUsage(MethodUsage methodUsage)
			{
				Console.WriteLine("Allocated {0} bytes in {1} allocations: {2}", methodUsage.AllocatedBytes, methodUsage.NumberOfAllocations, methodUsage.Name);
			}

			public void PrintCallstack(Callstack callstack, int callstackIndex, int numberOfMethods, string indentation)
			{
				Console.WriteLine("Callstack #{0} allocated {1} bytes.", callstackIndex, callstack.AllocatedBytes);
				Console.WriteLine(callstack.ToString(numberOfMethods, indentation + "    "));
			}

			public int Compare (CallstackUsage x, CallstackUsage y)
			{
				return CompareUlong(y.Callstack.AllocatedBytes, x.Callstack.AllocatedBytes);
			}

			public int Compare (MethodUsage x, MethodUsage y)
			{
				return CompareUlong(y.AllocatedBytes, x.AllocatedBytes);
			}

			public int Compare (Callstack x, Callstack y)
			{
				return CompareUlong(y.AllocatedBytes, x.AllocatedBytes);
			}
		}
	}
}

