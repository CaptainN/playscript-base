using System;

namespace PlayScript.Tools.TelemetryDump
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			if (args.Length == 0) {
				Console.WriteLine("Disassembles a telemetry file");
				Console.WriteLine("  Usage: TelemetryDump <telemetry-file> [<output-file>]");
				return;
			}

			string sourceFile = args[0];
			string destFile;
			if (args.Length >= 2) {
				destFile = args[1];
			} else {
				destFile = sourceFile + ".code";
			}

			try {
				Telemetry.Parser.ParseFile(sourceFile, destFile);
				Console.WriteLine("Successfully dumped telemetry {0} to {1}", sourceFile, destFile);
			}
			catch (Exception e) {
				Console.WriteLine("Error: {0}", e.ToString());
			}
		}
	}
}
