using System;
using System.Collections.Generic;

namespace PlayScript.Tools.ProfileCore
{
	public class StringPool
	{
		static Dictionary<string, string>	sStrings = new Dictionary<string, string>();

		public static string GetPooledString(string value)
		{
			string pooledString;
			if (sStrings.TryGetValue(value, out pooledString) == false)
			{
				sStrings.Add(value, value);
				pooledString = value;
			}
			return pooledString;
		}
	}
}

