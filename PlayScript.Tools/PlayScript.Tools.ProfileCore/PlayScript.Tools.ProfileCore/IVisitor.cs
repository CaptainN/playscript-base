using System;

namespace PlayScript.Tools.ProfileCore
{
	public interface IVisitor
	{
		void OnEnterMethod(ProfileInfo profileInfo);
		void OnLeaveMethod(ProfileInfo profileInfo);
	}

	public class BaseVisitor : IVisitor
	{
		public virtual void OnEnterMethod(ProfileInfo profileInfo)
		{
		}

		public virtual void OnLeaveMethod(ProfileInfo profileInfo)
		{
		}
	}
}

