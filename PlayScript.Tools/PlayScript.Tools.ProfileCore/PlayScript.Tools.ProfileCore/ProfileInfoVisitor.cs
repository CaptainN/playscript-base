using System;

namespace PlayScript.Tools.ProfileCore
{
	public static class ProfileInfoVisitor
	{
		public static void Visit(ProfileInfo topProfileInfo, IVisitor visitor)
		{
			visitor.OnEnterMethod(topProfileInfo);

			foreach ( ProfileInfo child in topProfileInfo.Callees)
			{
				Visit(child, visitor);
			}

			visitor.OnLeaveMethod(topProfileInfo);
		}
	}
}

