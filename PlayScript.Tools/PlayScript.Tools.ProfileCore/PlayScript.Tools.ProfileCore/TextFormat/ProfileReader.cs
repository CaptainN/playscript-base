using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;

namespace PlayScript.Tools.ProfileCore.TextFormat
{
	public static class ProfileReader
	{
		struct LineInfo
		{
			public enum Type
			{
				FrameStart,
				FrameEnd,
				MethodEnter,
				MethodLeave,
				Alloc,
			}
			public Type		LineType;
			public ulong	Time;

			/// <summary>
			/// Function name or allocated type.
			/// </summary>
			public string	Name;

			/// <summary>
			/// Allocated size or 0 if it is not an allocation.
			/// </summary>
			public int		AllocatedSize;

			public LineInfo(string line)
			{
				// This code is not very robust, could be improved at some point

				int indexOfFirstSpace = line.IndexOf(' ');
				if (indexOfFirstSpace <= 0)
				{
					throw new NotSupportedException();
				}

				string timeAsString = line.Substring(0, indexOfFirstSpace);
				if (timeAsString.EndsWith(":"))
				{
					timeAsString = timeAsString.Substring(0, timeAsString.Length - 1);
				}
				Time = ulong.Parse(timeAsString);

				int scanIndex = indexOfFirstSpace;

				// Skip the spaces
				while (line[++scanIndex] == ' ');

				int indexOfAction = scanIndex;
				int indexAfterAction = line.IndexOf(' ', indexOfAction);

				string action = line.Substring(indexOfAction, indexAfterAction - indexOfAction);
				switch (action)
				{
					case "start":
						LineType = Type.FrameStart;
						break;
					case "end":
						LineType = Type.FrameEnd;
						break;
					case "enter":
						LineType = Type.MethodEnter;
						break;
					case "leave":
						LineType = Type.MethodLeave;
						break;
					case "alloc":
						LineType = Type.Alloc;
						break;
					default:
						throw new NotSupportedException();
				}

				scanIndex = indexAfterAction;
				while(line[++scanIndex] == ' ');

				if (LineType == Type.Alloc)
				{
					// object:208691672 size:32 class:flash.events.Event
					string[] split = line.Substring(scanIndex).Split(' ');
					Debug.Assert(split.Length == 3);
					// Assume result size is 3
					// We skip "object:"
					Debug.Assert(split[0].StartsWith("object:"));
					Debug.Assert(split[1].StartsWith("size:"));
					Debug.Assert(split[2].StartsWith("class:"));
					// We just have to look at "size:" and "class:"
					AllocatedSize = int.Parse(split[1].Substring("size:".Length));
					Name = StringPool.GetPooledString(split[2].Substring("class:".Length));
				}
				else
				{
					AllocatedSize = 0;
					Name = StringPool.GetPooledString(line.Substring(scanIndex));
				}
			}
		}

		public static ProfileInfo ReadFiles(params string[] fileNames)
		{
			ProfileInfo topProfileInfo = new ProfileInfo("<TOP LEVEL>");
			topProfileInfo.IsFrame = true;
			foreach (string fileName in fileNames)
			{
				Console.WriteLine("Read file " +fileName);
				string[] lines = File.ReadAllLines(fileName);
				int currentPosition = 0;
				ulong frameTimeSpentInChild;
				ReadLineRecursively(lines, ref currentPosition, topProfileInfo, 0, out frameTimeSpentInChild);
				topProfileInfo.TimeSpentInclusively += frameTimeSpentInChild;
			}
			return topProfileInfo;
		}

		private static ulong ReadLineRecursively(string[] lines, ref int currentPosition, ProfileInfo parentProfileInfo, int depth, out ulong timeSpentInChild)
		{
			// TODO: Refactor this to avoid code duplication and make the code more straightforward
			//			Enter is detected at level N, leave is detected at level N + 1, inclusive / exclusive management spans both making things more complicated than it could be

			timeSpentInChild = 0;

			while (currentPosition < lines.Length)
			{
				string oneLine = lines[currentPosition++];
				LineInfo lineInfo = new LineInfo(oneLine);

				switch (lineInfo.LineType)
				{
					case LineInfo.Type.FrameStart:
						{
							ProfileInfo frameProfileInfo = new ProfileInfo(lineInfo.Name);
							frameProfileInfo.IsFrame = true;
							parentProfileInfo.AddCallee(frameProfileInfo);
							frameProfileInfo.AddCaller(parentProfileInfo);

							ulong frameTimeSpentInChild;
							ulong firstTimeRecorded = lineInfo.Time;
							ulong lastTimeRecorded = ReadLineRecursively(lines, ref currentPosition, frameProfileInfo, depth + 1, out frameTimeSpentInChild);
							ulong inclusiveChildTime = lastTimeRecorded - firstTimeRecorded;
							timeSpentInChild += inclusiveChildTime;
							frameProfileInfo.TimeSpentInclusively += inclusiveChildTime;
							ulong exclusiveTime = inclusiveChildTime - frameTimeSpentInChild;
							frameProfileInfo.TimeSpentExclusively += exclusiveTime;

							// Now that we finished parsing the frame, let's consolidate to save memory
							//	Creates an issue with callstack matching, disabled for the moment - was not saving enough memory anyway...
							//	frameProfileInfo.Consolidate();
							break;
						}
					case LineInfo.Type.FrameEnd:
						return lineInfo.Time;

					case LineInfo.Type.MethodEnter:
						{
							ProfileInfo childProfileInfo = new ProfileInfo(lineInfo.Name);
							childProfileInfo.NumberOfCalls = 1;
							parentProfileInfo.AddCallee(childProfileInfo);
							childProfileInfo.AddCaller(parentProfileInfo);
							
							ulong childTimeSpentInChild;
							ulong firstTimeRecorded = lineInfo.Time;
							ulong lastTimeRecorded = ReadLineRecursively(lines, ref currentPosition, childProfileInfo, depth + 1, out childTimeSpentInChild);
							ulong inclusiveChildTime = lastTimeRecorded - firstTimeRecorded;
							timeSpentInChild += inclusiveChildTime;
							childProfileInfo.TimeSpentInclusively += inclusiveChildTime;
							ulong exclusiveTime = inclusiveChildTime - childTimeSpentInChild;
							childProfileInfo.TimeSpentExclusively += exclusiveTime;
							break;
						}
					case LineInfo.Type.MethodLeave:
						// If we reach leave, it means that we have to go back to the parent
						return lineInfo.Time;

					case LineInfo.Type.Alloc:
						{
							if (parentProfileInfo.Allocations == null)
							{
								parentProfileInfo.Allocations = new List<AllocInfo>();
							}
							AllocInfo allocInfo = new AllocInfo(lineInfo.Name, lineInfo.AllocatedSize);
							parentProfileInfo.Allocations.Add(allocInfo);
						}
						break;

					default:
						throw new NotSupportedException();
				}
			}

			// File is incomplete? Not the same number of bad frames.
			if (depth != 0)
			{
				throw new NotSupportedException();
			}

			// We reach the end of the frame, this counter should not matter at this point
			return 0;
		}
	}
}

