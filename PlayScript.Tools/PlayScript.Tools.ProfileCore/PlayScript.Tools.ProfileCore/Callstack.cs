using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

namespace PlayScript.Tools.ProfileCore
{
	public class Callstack
	{
		public List<KeyValuePair<string,ProfileInfo>>	Names = new List<KeyValuePair<string, ProfileInfo>>();
		public int										Usage;
		public ulong									TimeSpentExclusively;
		public ulong									TimeSpentInclusively;
		public ulong									AllocatedBytes;
		public int										NumberOfAllocations;

		public Callstack(ProfileInfo profileInfo)
		{
			do
			{
				string name = profileInfo.Name;
				Names.Add(new KeyValuePair<string, ProfileInfo>(name, profileInfo));

				// We can calculate callstack only on profile info that have not been merged with different callers
				Debug.Assert(profileInfo.Callers.Length == 1);
				profileInfo = profileInfo.Callers[0];
			}
			while (profileInfo.IsFrame == false);		// And we stop when it is a frame (we don't want it to be part of the callstack as the name is different anyway)
		}

		public override int GetHashCode ()
		{
			int hashCode = 0;
			foreach (KeyValuePair<string, ProfileInfo> kvp in Names)
			{
				hashCode ^= kvp.Key.GetHashCode();
			}
			return hashCode;
		}

		public override bool Equals (object obj)
		{
			Callstack otherCallstack = obj as Callstack;
			if (otherCallstack == null)
			{
				return false;
			}

			int count = Names.Count;
			if (count != otherCallstack.Names.Count)
			{
				return false;
			}

			for (int i = 0 ; i < count ; ++i)
			{
				if (Names[i].Key != otherCallstack.Names[i].Key)
				{
					return false;
				}
			}
			return true;
		}

		public string ToString(int numberOfMethods, string indent)
		{
			sBuilder.Length = 0;
			for (int i = 0 ; i < Names.Count ; ++i)
			{
				if (i > numberOfMethods)
				{
					break;
				}
				sBuilder.AppendLine(indent + Names[i].Key);
			}
			return sBuilder.ToString();
		}

		static StringBuilder sBuilder = new StringBuilder();
	}
}

