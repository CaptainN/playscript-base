using System;
using System.Collections.Generic;

namespace PlayScript.Tools.ProfileCore
{
	public class AllocInfo
	{
		public AllocInfo(string allocatedType, int size)
		{
			AllocatedSize = size;
			AllocatedType = allocatedType;
		}

		public int		AllocatedSize;
		public string	AllocatedType;
	}

	public class ProfileInfo
	{
		public bool				IsFrame { get; set; }
		public string			Name { get; private set; }
		public ulong			TimeSpentExclusively { get; set; }
		public ulong			TimeSpentInclusively { get; set; }
		public int				NumberOfCalls { get; set; }

		public List<AllocInfo>	Allocations;

		private ProfileInfo[]	mCachedCallers;
		private ProfileInfo[]	mCachedCallees;

		private static ProfileInfo[]	sEmptyArray = new ProfileInfo[0];

		public ProfileInfo[]	Callers
		{
			get
			{
				if (mCachedCallers == null)
				{
					if (mCallers != null)
					{
						mCachedCallers = mCallers.ToArray();
					}
					else
					{
						mCachedCallers = sEmptyArray;
					}
				}
				return mCachedCallers;
			}
		}
		public ProfileInfo[]	Callees
		{
			get
			{
				if (mCachedCallees == null)
				{
					if (mCallees != null)
					{
						mCachedCallees = mCallees.ToArray();
					}
					else
					{
						mCachedCallees = sEmptyArray;
					}
				}
				return mCachedCallees;
			}
		}

		public bool IsLeaf
		{
			get
			{
				return (mCallees == null) || (mCallees.Count == 0);
			}
		}

		private List<ProfileInfo>	mCallees;
		private List<ProfileInfo>	mCallers;

		public ProfileInfo (string name)
		{
			Name = name;
		}

		public void AddCallee(ProfileInfo callee)
		{
			mCachedCallees = null;

			if (mCallees == null)
			{
				mCallees = new List<ProfileInfo>();
			}
			mCallees.Add(callee);
		}

		public void AddCaller(ProfileInfo caller)
		{
			mCachedCallers = null;

			if (mCallers == null)
			{
				mCallers = new List<ProfileInfo>();
			}
			mCallers.Add(caller);
		}

		public override string ToString ()
		{
			return string.Format ("[ProfileInfo: Name={0}, NumberOfCalls={1}]", Name, NumberOfCalls);
		}

		public void Merge(ProfileInfo profileInfo)
		{
			NumberOfCalls += profileInfo.NumberOfCalls;
			TimeSpentExclusively += profileInfo.TimeSpentExclusively;
			TimeSpentInclusively += profileInfo.TimeSpentInclusively;

			if (profileInfo.mCallers != null)
			{
				foreach (ProfileInfo parent in profileInfo.mCallers)
				{
					AddCaller(parent);
				}
			}
			if (profileInfo.mCallees != null)
			{
				foreach (ProfileInfo child in profileInfo.mCallees)
				{
					AddCallee(child);
				}
			}
		}

		public ProfileInfo Clone()
		{
			ProfileInfo clonedProfileInfo = new ProfileInfo(Name);
			clonedProfileInfo.TimeSpentExclusively = TimeSpentExclusively;
			clonedProfileInfo.TimeSpentInclusively = TimeSpentInclusively;
			clonedProfileInfo.NumberOfCalls = NumberOfCalls;
			if (mCallers != null)
			{
				foreach (ProfileInfo parent in mCallers)
				{
					clonedProfileInfo.AddCaller(parent);
				}
			}
			if (mCallees != null)
			{
				foreach (ProfileInfo child in mCallees)
				{
					clonedProfileInfo.AddCallee(child);
				}
			}
			return clonedProfileInfo;
		}

		public static ProfileInfo[] MergeSame(IEnumerable<ProfileInfo> profileInfos)
		{
			Dictionary<string, ProfileInfo> allProfileInfos = new Dictionary<string, ProfileInfo>();
			foreach (ProfileInfo profileInfoToMerge in profileInfos)
			{
				ProfileInfo mergedProfileInfo;
				if (allProfileInfos.TryGetValue(profileInfoToMerge.Name, out mergedProfileInfo) == false)
				{
					mergedProfileInfo = new ProfileInfo(profileInfoToMerge.Name);
					allProfileInfos.Add(profileInfoToMerge.Name, mergedProfileInfo);
				}
				mergedProfileInfo.Merge(profileInfoToMerge);
			}
			return ToArray(allProfileInfos.Values);
		}

		/// <summary>
		/// Recursively consolidate the profile info instance.
		/// 
		/// The goal is to merge children so we can save memory.
		/// Merging can be as simple as just merging the same leave nodes,
		/// the best would be to merge up until we only have unique callstacks.
		/// </summary>
		public void Consolidate()
		{
			if (mCallees != null)
			{
				foreach (ProfileInfo child in mCallees)
				{
					child.Consolidate();
				}

				// Now that we consolidated the children, let's check if there are some leaf children that we could merge
				List<ProfileInfo> nonLeafChildren = new List<ProfileInfo>(mCallees.Count);
				List<ProfileInfo> leafChildren = new List<ProfileInfo>(mCallees.Count);

				foreach (ProfileInfo child in mCallees)
				{
					if (child.IsLeaf)
					{
						leafChildren.Add(child);
					}
					else
					{
						// Note that we could also consolidate non-leaf children at the condition that they represent the exact same callstack
						// How should we detect this efficiently?
						nonLeafChildren.Add(child);
					}
				}

				if (leafChildren.Count > 1)
				{
					ProfileInfo[] mergedLeafChildren = MergeSame(leafChildren);
					if (mergedLeafChildren.Length != leafChildren.Count)
					{
						// We were able to merge some of the leaf children, so let's update the profile info
						nonLeafChildren.AddRange(mergedLeafChildren);
						mCallees = nonLeafChildren;
						mCachedCallees = null;
					}
				}
			}
		}

		public static ProfileInfo[] ToArray(ICollection<ProfileInfo> profileInfos)
		{
			ProfileInfo[] profileInfosAsArray = new ProfileInfo[profileInfos.Count];
			profileInfos.CopyTo(profileInfosAsArray, 0);
			return profileInfosAsArray;
		}

		public static void SortByNumberOfCalls(ProfileInfo[] profileInfos)
		{
			Array.Sort(profileInfos, new NumberOfCallsComparer());
		}

		class NumberOfCallsComparer : IComparer<ProfileInfo>
		{
			public int Compare (ProfileInfo x, ProfileInfo y)
			{
				// Descending order
				return y.NumberOfCalls - x.NumberOfCalls;
			}
		}
	}
}

